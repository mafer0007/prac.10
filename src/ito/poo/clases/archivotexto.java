package ito.poo.clases;
import java.util.Formatter;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class archivotexto {
	public static void grabaDatos(ArrayList<Cuenta> l) throws FileNotFoundException {
		Formatter archivo;
		
		archivo = new Formatter(new File("datos.txt"));
		for(Cuenta cu :l) {
			archivo.format("%d;%s;%l;%f;%l\n",cu.getNumeroDC(),cu.getCliente(),cu.getFechaApertura(),cu.getSaldo(),cu.getFechaUltAct());
		}
		
		archivo.close();
	}
	
	public static ArrayList<Cuenta> leerDatos() throws FileNotFoundException{
		ArrayList<Cuenta> l =new ArrayList<Cuenta>();
		Scanner file=new Scanner(new File("datos.txt"));
		while(file.hasNext()) {
			Scanner linea = new Scanner(file.nextLine());
			linea.useDelimiter(";");
			int numeroDC = linea.nextInt();
			String cliente = linea.next();
			LocalDate fechaApertura = null;
			float saldo = linea.nextFloat();
			LocalDate fechaUltAct = null;

			l.add(new Cuenta(numeroDC,cliente,fechaApertura,saldo,fechaUltAct));
		}
		return l;
	}

}


